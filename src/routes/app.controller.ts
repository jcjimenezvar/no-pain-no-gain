import * as express from "express";
import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  interfaces,
  next,
  request,
  response,
} from "inversify-express-utils";
import { inject } from "../config/ioc/inversify.config";
import { TYPES } from "../config/ioc/types";
import { Response } from "../models/response/Reponse.model";
import { ICreateCampusService } from "../services/create-campus";
import { ICreateCityService } from "../services/create-city";
import { ICreateUserService } from "../services/create-user";
import { IGetUsersService } from "../services/get-users";
import { ILoginService } from "../services/login";
import {
  createCampus,
  createCity,
  createUser,
  getUsers,
  login,
} from "./schemas";

@controller("")
export class AppController implements interfaces.Controller {
  constructor(
    @inject(TYPES.ICreateUser)
    private createUserService: ICreateUserService,
    @inject(TYPES.ILoginService)
    private loginService: ILoginService,
    @inject(TYPES.ICreateCityService)
    private createCityService: ICreateCityService,
    @inject(TYPES.ICreateCampusService)
    private createCampusService: ICreateCampusService,
    @inject(TYPES.IGetUsersService)
    private getUsersService: IGetUsersService
  ) {}
  @httpPut("/create-user")
  public async createUser(
    @request() req: express.Request,
    @response() res: express.Response,
    @next() nextFunc: express.NextFunction
  ) {
    try {
      const data = req.body;
      const validationResult = createUser.validate(data);
      let httpResponse: Response = {
        data: null,
        errors: [],
      };
      if (validationResult.error) {
        // Si la validación de los datos de la solicitud falla, entonces retorna error 422 / Invalid Request
        httpResponse = {
          data: null,
          errors: ["invalid_request"],
        };
        res.status(422).send(httpResponse);
        nextFunc();
      } else {
        const response: Response = await this.createUserService.put(data);
        if (response.errors.length) {
          res.status(400).send(response);
        }
        res.status(200).send(response);
      }
    } catch (error) {
      res.status(500).send({ error });
    }
  }

  @httpPost("/login")
  public async login(
    @request() req: express.Request,
    @response() res: express.Response,
    @next() nextFunc: express.NextFunction
  ) {
    try {
      const data = req.body;
      const validationResult = login.validate(data);
      let httpResponse: Response = {
        data: null,
        errors: [],
      };
      if (validationResult.error) {
        // Si la validación de los datos de la solicitud falla, entonces retorna error 422 / Invalid Request
        httpResponse = {
          data: null,
          errors: ["invalid_request"],
        };
        res.status(422).send(httpResponse);
        nextFunc();
      } else {
        const response: Response = await this.loginService.login(data);
        if (response.errors.length) {
          res.status(400).send(response);
          nextFunc();
        }
        res.status(200).send(response);
        nextFunc();
      }
    } catch (error) {
      res.status(500).send({ error });
      nextFunc();
    }
  }

  @httpPut("/create-city")
  public async createCity(
    @request() req: express.Request,
    @response() res: express.Response,
    @next() nextFunc: express.NextFunction
  ) {
    try {
      const data = req.body;
      const validationResult = createCity.validate(data);
      let httpResponse: Response = {
        data: null,
        errors: [],
      };
      if (validationResult.error) {
        // Si la validación de los datos de la solicitud falla, entonces retorna error 422 / Invalid Request
        httpResponse = {
          data: null,
          errors: ["invalid_request"],
        };
        res.status(422).send(httpResponse);
        nextFunc();
      } else {
        const response: Response = await this.createCityService.put(data);
        if (response.errors.length) {
          res.status(400).send(response);
          nextFunc();
        }
        res.status(200).send(response);
      }
    } catch (error) {
      res.status(500).send({ error });
      nextFunc();
    }
  }

  @httpPut("/create-campus")
  public async createCampus(
    @request() req: express.Request,
    @response() res: express.Response,
    @next() nextFunc: express.NextFunction
  ) {
    try {
      const data = req.body;
      const validationResult = createCampus.validate(data);
      let httpResponse: Response = {
        data: null,
        errors: [],
      };
      if (validationResult.error) {
        // Si la validación de los datos de la solicitud falla, entonces retorna error 422 / Invalid Request
        httpResponse = {
          data: null,
          errors: ["invalid_request"],
        };
        res.status(422).send(httpResponse);
        nextFunc();
      } else {
        const response: Response = await this.createCampusService.put(data);
        if (response.errors.length) {
          res.status(400).send(response);
          nextFunc();
        }
        res.status(200).send(response);
      }
    } catch (error) {
      res.status(500).send({ error });
      nextFunc();
    }
  }

  @httpGet("/get-users/:cityId/:campusId")
  public async getUsers(
    @request() req: express.Request,
    @response() res: express.Response,
    @next() nextFunc: express.NextFunction
  ) {
    try {
      const paramsData = req.params;
      const data = req.body;
      const validationResult = getUsers.validate({
        ...paramsData,
        ...data,
      });
      let httpResponse: Response = {
        data: null,
        errors: [],
      };
      if (validationResult.error) {
        // Si la validación de los datos de la solicitud falla, entonces retorna error 422 / Invalid Request
        httpResponse = {
          data: null,
          errors: ["invalid_request"],
        };
        res.status(422).send(httpResponse);        
      } else {
        const response: Response = await this.getUsersService.get({
          ...paramsData,
          ...data,
        });
        if (response.errors.length) {
          res.status(400).send(response);
        }
        res.status(200).send(response);
      }
    } catch (error) {
      res.status(500).send({ error });
    }
  }
}
