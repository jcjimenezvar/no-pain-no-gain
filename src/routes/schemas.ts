import joi from "joi";

export const createUser = joi
  .object()
  .keys({
    admin: joi.boolean().required(),
    campusId: joi.number().required(),
    cityId: joi.number().required(),
    email: joi.string().required(),
    fullname: joi.string().required(),
    password: joi.string().required(),
    username: joi.string().required(),
  })
  .required();

export const login = joi
  .object()
  .keys({
    email: joi.string().required(),
    password: joi.string().required(),
  })
  .required();

export const createCity = joi
  .object()
  .keys({
    cityId: joi.number().required(),
    cityName: joi.string().required(),
    email: joi.string().required(),
  })
  .required();

export const createCampus = joi
  .object()
  .keys({
    campusAddress: joi.string().required(),
    campusId: joi.number().required(),
    campusName: joi.string().required(),
    cityId: joi.number().required(),
    email: joi.string().required(),
  })
  .required();

export const getUsers = joi
  .object()
  .keys({
    campusId: joi.number().required(),
    cityId: joi.number().required(),
    email: joi.string().required(),
  })
  .required();
