import { ICreateUserService } from '.'
import { injectable } from '../../config/ioc/inversify.config'
import { Users } from '../../models/db-models/users.model'
import { UsersRequest } from '../../models/request-models/users-request.model'

@injectable()
export class CreateUserService implements ICreateUserService {
  public put = async (payload: UsersRequest) => {
    if (await this.validateIfUserExists(payload.email)) {
      return {
        data: {},
        errors: [`El usuario con email ${payload.email} ya esta registrado`],
      }
    }
    const newUser = await this.create(payload)

    const response: any = {
      data: newUser,
      errors: [],
    }
    return response
  }

  private validateIfUserExists = async (email: string) => {
    let exists: boolean = false
    const user = await Users.findOne({
      raw: true,
      where: {
        email,
      },
    })

    if (user) exists = true

    return exists
  }

  private create = async (payload: UsersRequest) => {
    return await Users.create({
      admin: payload.admin,
      campus_id: payload.campusId,
      city_id: payload.cityId,
      email: payload.email,
      fullname: payload.fullname,
      password: payload.password,
      username: payload.username,
    })
  }
}
