import { ICreateCityService } from '.'
import { injectable } from '../../config/ioc/inversify.config'
import { City } from '../../models/db-models/city.model'
import { Users } from '../../models/db-models/users.model'
import { CityRequest } from '../../models/request-models/city-request.model'

@injectable()
export class CreateCityService implements ICreateCityService {
  public put = async (payload: CityRequest) => {
    if (await this.validateIfCityExists(payload.cityId)) {
      return {
        data: {},
        errors: [`La ciudad ${payload.cityId} ya existe`],
      }
    }

    if (!(await this.validateIfUserIsAdminAndIsLogged(payload.email))) {
      return {
        data: {},
        errors: [
          `El usuario ${payload.email} no esta autorizado para realizar esta acción`,
        ],
      }
    }

    const newCity = await this.create(payload)

    const response: any = {
      data: newCity,
      errors: [],
    }
    return response
  }

  private validateIfUserIsAdminAndIsLogged = async (email: string) => {
    let isAdminAndIsLogged: boolean = false
    const user = await Users.findOne({
      raw: true,
      where: {
        admin: true,
        email: email,
        logged_on: true,
      },
    })

    if (user) isAdminAndIsLogged = true

    return isAdminAndIsLogged
  }

  private validateIfCityExists = async (cityId: number) => {
    let exists: boolean = false
    const city = await City.findOne({
      raw: true,
      where: {
        city_id: cityId,
      },
    })

    if (city) exists = true

    return exists
  }

  private create = async (payload: CityRequest) => {
    return await City.create({
      city_id: payload.cityId,
      city_name: payload.cityName,
    })
  }
}
