import { ICreateCampusService } from '.'
import { injectable } from '../../config/ioc/inversify.config'
import { Campus } from '../../models/db-models/campus.model'
import { City } from '../../models/db-models/city.model'
import { Users } from '../../models/db-models/users.model'
import { CampusRequest } from '../../models/request-models/campus-request.model'

@injectable()
export class CreateCampusService implements ICreateCampusService {
  public put = async (payload: CampusRequest) => {
    if (await this.validateIfCampusExists(payload.campusId, payload.campusId)) {
      return {
        data: {},
        errors: [
          `La sede ${payload.campusName} que intenta crear ya existe en la ciudad ${payload.cityId}`,
        ],
      }
    }

    if (!(await this.validateIfCityExists(payload.cityId))) {
      return {
        data: {},
        errors: [
          `La ciudad ${payload.cityId} en la que intenta registrar la sede no existe`,
        ],
      }
    }

    if (!(await this.validateIfUserIsAdminAndIsLogged(payload.email))) {
      return {
        data: {},
        errors: [
          `El usuario ${payload.email} no esta autorizado para realizar esta acción`,
        ],
      }
    }

    const newCampus = await this.create(payload)

    const response: any = {
      data: newCampus,
      errors: [],
    }
    return response
  }

  private validateIfUserIsAdminAndIsLogged = async (email: string) => {
    let isAdminAndIsLogged: boolean = false
    const user = await Users.findOne({
      raw: true,
      where: {        
        admin: true,
        email: email,
        logged_on: true,
      },
    })

    if (user) isAdminAndIsLogged = true

    return isAdminAndIsLogged
  }

  private validateIfCityExists = async (cityId: number) => {
    let exists: boolean = true
    const city = await City.findOne({
      raw: true,
      where: {
        city_id: cityId,
      },
    })

    if (!city) exists = false

    return exists
  }

  private validateIfCampusExists = async (cityId: number, campusId: number) => {
    let exists: boolean = false
    const campus = await Campus.findOne({
      raw: true,
      where: {
        campus_id: campusId,
        city_id: cityId,
      },
    })

    if (campus) exists = true

    return exists
  }

  private create = async (payload: CampusRequest) => {
    return await Campus.create({
      campus_address: payload.campusAddress,
      campus_id: payload.campusId,
      campus_name: payload.campusName,
      city_id: payload.cityId,
    })
  }
}
