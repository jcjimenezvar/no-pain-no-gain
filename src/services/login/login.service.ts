import { ILoginService } from ".";
import { injectable } from "../../config/ioc/inversify.config";
import { Users } from "../../models/db-models/users.model";
import { LoginRequest } from "../../models/request-models/login-request.model";

@injectable()
export class LoginService implements ILoginService {
  
  public login = async (payload: LoginRequest) => {
    if (!(await this.validateIfUserExists(payload.email, payload.password))) {
      return {
        data: {},
        errors: [
          `El usuario con email ${payload.email} no existe o la contraseña no es correcta`,
        ],
      };
    }

    const updatedUser = await this.update(payload);    

    if(updatedUser[0]===0){
      return {
        data: {},
        errors: [
          `Ocurrio un error actualizando la información del usuario`,
        ],
      };
    }

    const response: any = {
      data: await this.get(payload.email),
      errors: [],
    };
    return response;
  };

  private validateIfUserExists = async (email: string, password: string) => {
    const user = await Users.findOne({
      raw: true,
      where: {
        email,
      },
    });

    if (!user) {
      return false;
    }

    if (user && password !== user.password) {
      return false;
    }

    return user;
  };

  private update = async (payload: LoginRequest) => {
    return await Users.update(
      {
        logged_on: true,
      },
      {
        where: {
          email: payload.email,
        },
      }
    );
  };

  private get = async (email: string) => {
    return await Users.findOne({
      where: {
        email,
      },
    });
  };
}
