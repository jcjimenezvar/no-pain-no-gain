import { IGetUsersService } from '.'
import { injectable } from '../../config/ioc/inversify.config'
import { Campus } from '../../models/db-models/campus.model'
import { City } from '../../models/db-models/city.model'
import { Users } from '../../models/db-models/users.model'
import { GetUsersRequest } from '../../models/request-models/get-users-request.model'

@injectable()
export class GetUsersService implements IGetUsersService {
  public get = async (payload: GetUsersRequest) => {
    if (
      !(await this.validateIfCampusExists(payload.campusId, payload.campusId))
    ) {
      return {
        data: {},
        errors: [`La sede ${payload.campusId} no existe`],
      }
    }

    if (!(await this.validateIfCityExists(payload.cityId))) {
      return {
        data: {},
        errors: [`La ciudad ${payload.cityId} no existe`],
      }
    }

    if (!(await this.validateIfUserIsAdminAndIsLogged(payload.email))) {
      return {
        data: {},
        errors: [
          `El usuario ${payload.email} no esta autorizado para realizar esta acción`,
        ],
      }
    }

    const response: any = {
      data: await this.getAllUsers(payload.cityId, payload.campusId),
      errors: [],
    }
    return response
  }

  private validateIfUserIsAdminAndIsLogged = async (email: string) => {
    let isAdminAndIsLogged: boolean = false
    const user = await Users.findOne({
      raw: true,
      where: {
        email: email,
        admin: true,
        logged_on: true,
      },
    })

    if (user) isAdminAndIsLogged = true

    return isAdminAndIsLogged
  }

  private validateIfCityExists = async (cityId: number) => {
    let exists: boolean = true
    const city = await City.findOne({
      raw: true,
      where: {
        city_id: cityId,
      },
    })

    if (!city) exists = false

    return exists
  }

  private validateIfCampusExists = async (cityId: number, campusId: number) => {
    let exists: boolean = true
    const campus = await Campus.findOne({
      raw: true,
      where: {
        campus_id: campusId,
        city_id: cityId,
      },
    })

    if (!campus) exists = false

    return exists
  }

  private getAllUsers = async (cityId: number, campusId: number) => {
    return await Users.findAll({
      raw: true,
      where: {
        campus_id: campusId,
        city_id: cityId,
      },
    })
  }
}
