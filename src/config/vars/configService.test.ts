import 'reflect-metadata'

const vars = {
  nodeEnv: 'NODE_ENV',
  nodePort: 'NODE_PORT',
  port: 'PORT',
  rootPath: 'ROOT_PATH',
  host: 'DB_HOST',
  name: 'DATABASE',
  password: 'DB_PASSWORD',
  dbPort: 'DB_PORT',
  username: 'DB_USERNAME',
}

import { ConfigService } from './configService'

describe('ConfigService', () => {
  const config = new ConfigService()

  test('getVars devuelve undefined antes de llamar a load', () => {
    const env = config.getVars()
    expect(env).toBeUndefined()
  })

  test('load da error de validación si no estan configuradas las variables requeridas', () => {
    const err = config.load()
    expect(err).toContain('Config validation error:')
  })

  test('load NO da error de validación si estan configuradas las variables requeridas', () => {
    process.env[vars.nodeEnv] = 'stg'
    process.env[vars.rootPath] = 'some-path'
    process.env[vars.dbPort] = '3306'
    process.env[vars.host] = 'localhost'
    process.env[vars.name] = 'dbname'
    process.env[vars.password] = 'pass'
    process.env[vars.username] = 'username'
    process.env[vars.nodePort] = '4000'

    const err = config.load()
    expect(err).toBeFalsy()
    expect(config.getVars()).toBeTruthy()
  })

  test('NODE_ENV invalido da error', () => {
    process.env[vars.nodeEnv] = 'invalid-value'

    const err = config.load()
    expect(err).toContain('Config validation error:')
    expect(config.getVars()).toBeFalsy()
  })

  test('NODE_ENV development carga correctamente', () => {
    process.env[vars.nodeEnv] = 'dev'

    const err = config.load()
    expect(err).toBeFalsy()
    expect(config.getVars().env).toEqual('dev')
    expect(config.getVars().isDev).toBeTruthy()
  })

  test('NODE_ENV testing carga correctamente', () => {
    process.env[vars.nodeEnv] = 'stg'

    const err = config.load()
    expect(err).toBeFalsy()
    expect(config.getVars().env).toEqual('stg')
    expect(config.getVars().isTest).toBeTruthy()
  })

  test('NODE_ENV production carga correctamente', () => {
    process.env[vars.nodeEnv] = 'prod'

    const err = config.load()
    expect(err).toBeFalsy()
    expect(config.getVars().env).toEqual('prod')
    expect(config.getVars().isProd).toBeTruthy()
  })

  test('PORT carga correctamente', () => {
    process.env[vars.port] = '4000'

    const err = config.load()
    expect(err).toBeFalsy()
    expect(config.getVars().server.port).toEqual(4000)
  })

  test('ROOT_PATH carga correctamente', () => {
    process.env[vars.rootPath] = 'some-123-path'
    const err = config.load()
    expect(err).toBeFalsy()
    expect(config.getVars().server.rootPath).toEqual('some-123-path')
  })
})
