// Validaciones de variables de entorno de configuración del microservicio.
import joi from "joi";

import { IConfig } from ".";

export class ConfigService implements IConfig {
  private vars: any;

  constructor() {
    this.vars = undefined;
  }

  /**
   * Retorna las variables de ambientes cargadas
   * @returns {any} Objeto con variables de ambiente
   */
  public getVars = () => {
    return this.vars;
  };

  /**
   * Lee las variables de ambientes, valida su contenido y las carga en servicio de configuracion.
   *
   * @returns {string} Error - En caseo de ocurrir errores de validación, devuelve un mensaje de error, caso contrario retorna null.
   */
  public load = () => {
    const envVarsSchema = joi
      .object({
        DATABASE: joi.string().required(),
        DB_HOST: joi.string().required(),
        DB_PASSWORD: joi.string().required(),
        DB_PORT: joi.number().required(),
        DB_USERNAME: joi.string().required(),
        NODE_ENV: joi.string().valid("dev", "stg", "prod").required(),
        NODE_PORT: joi.number().required(),
        ROOT_PATH: joi.string().required(),
      })
      .unknown()
      .required();

    const validation = envVarsSchema.validate(process.env, {
      allowUnknown: true,
    });
    if (validation.error) {
      this.vars = undefined;
      return `Config validation error: ${validation.error}`;
    }
    this.vars = {
      env: process.env.NODE_ENV,
      isDev: process.env.NODE_ENV === "dev",
      isProd: process.env.NODE_ENV === "prod",
      isTest: process.env.NODE_ENV === "stg",

      server: {
        port: Number(process.env.NODE_PORT),
        rootPath: process.env.ROOT_PATH,
      },

      database: {
        host: process.env.DB_HOST,
        name: process.env.DATABASE,
        password: process.env.DB_PASSWORD,
        port: process.env.DB_PORT,
        username: process.env.DB_USERNAME,
      },
    };
    return null;
  };
}
