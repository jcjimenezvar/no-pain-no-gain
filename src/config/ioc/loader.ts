import { buildProviderModule, container } from "./inversify.config";

/* REST Controllers */
import "../../routes/app.controller";

/* Services */

import "../../services/create-user/createUser.service";
import "../../services/login/login.service";

container.load(buildProviderModule());
