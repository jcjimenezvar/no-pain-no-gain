export const TYPES = {
  IConfig: Symbol.for("IConfig"),
  ICreateCampusService: Symbol.for("ICreateCampusService"),
  ICreateCityService: Symbol.for("ICreateCityService"),
  ICreateUser: Symbol.for("ICreateUser"),
  IGetUsersService: Symbol.for("IGetUsersService"),
  ILoginService: Symbol.for("ILoginService"),
};
