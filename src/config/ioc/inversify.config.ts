import { Container, inject, injectable } from "inversify";
import { buildProviderModule, provide } from "inversify-binding-decorators";
import { CreateCampusService } from "../../services/create-campus/createCampus.service";
import { CreateCityService } from "../../services/create-city/createCity.service";
import { CreateUserService } from "../../services/create-user/createUser.service";
import { GetUsersService } from "../../services/get-users/getUsers.service";
import { LoginService } from "../../services/login/login.service";
import { TYPES } from "./types";

const container = new Container();

container
  .bind<CreateUserService>(TYPES.ICreateUser)
  .to(CreateUserService)
  .inSingletonScope();

container
  .bind<LoginService>(TYPES.ILoginService)
  .to(LoginService)
  .inSingletonScope();

container
  .bind<CreateCityService>(TYPES.ICreateCityService)
  .to(CreateCityService)
  .inSingletonScope();

container
  .bind<CreateCampusService>(TYPES.ICreateCampusService)
  .to(CreateCampusService)
  .inSingletonScope();

container
  .bind<GetUsersService>(TYPES.IGetUsersService)
  .to(GetUsersService)
  .inSingletonScope();

export { buildProviderModule, container, provide, inject, injectable };
