import { Sequelize } from 'sequelize'
import { IConfig } from '../vars'

export class DBConnection {
  constructor(private config: IConfig) {}
  public connect = async () => {
    try {
      const db = this.config.getVars().database.name
      const username = this.config.getVars().database.username
      const password = this.config.getVars().database.password

      const sequelize = new Sequelize(db, username, password, {
        dialect: 'mysql',
        host: this.config.getVars().database.host,
        port: parseInt(String(this.config.getVars().database.port), 0),

        define: {
          underscored: true,
        },
      })
      module.exports = sequelize
      return sequelize
    } catch (error) {
      throw new Error(error)
    }
  }
}
