import { DataTypes, Model, Sequelize } from 'sequelize'

const db = String(process.env.DATABASE)
const username = String(process.env.DB_USERNAME)
const password = String(process.env.DB_PASSWORD)

const sequelize = new Sequelize(db, username, password, {
  define: {
    underscored: true,
  },
  dialect: 'mysql',
  host: String(process.env.DB_HOST),
  port: parseInt(String(process.env.DB_PORT), 0),
})

export class Campus extends Model {
  public campus_address!: string
  public campus_id!: number
  public campus_name!: string
  public city_id!: string
}

Campus.init(
  {
    campus_address: {
      allowNull: false,
      type: new DataTypes.STRING(255),
    },
    campus_id: {
      allowNull: false,
      type: new DataTypes.NUMBER(),
    },
    campus_name: {
      allowNull: false,
      type: new DataTypes.STRING(255),
    },

    city_id: {
      allowNull: false,
      type: new DataTypes.NUMBER(),
    },
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER.UNSIGNED,
    },
  },
  {
    sequelize,
    tableName: 'campus',
  }
)
