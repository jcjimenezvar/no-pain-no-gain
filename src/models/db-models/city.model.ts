import { DataTypes, Model, Sequelize } from "sequelize";

const db = String(process.env.DATABASE);
const username = String(process.env.DB_USERNAME);
const password = String(process.env.DB_PASSWORD);

const sequelize = new Sequelize(db, username, password, {
  host: String(process.env.DB_HOST),
  dialect: "mysql",
  port: parseInt(String(process.env.DB_PORT)),

  define: {
    underscored: true,
  },
});

export class City extends Model {
  public city_id!: number;
  public city_name!: string;
}

City.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false,
    },
    city_id: {
      type: new DataTypes.NUMBER(),
      allowNull: false,
    },
    city_name: {
      type: new DataTypes.STRING(255),
      allowNull: false,
    },
  },
  {
    tableName: "city",
    sequelize,
  }
);
