import { DataTypes, Model, Sequelize } from "sequelize";

const db = String(process.env.DATABASE);
const username = String(process.env.DB_USERNAME);
const password = String(process.env.DB_PASSWORD);

const sequelize = new Sequelize(db, username, password, {
  host: String(process.env.DB_HOST),
  dialect: "mysql",
  port: parseInt(String(process.env.DB_PORT)),

  define: {
    underscored: true,
  },
});

export class Users extends Model {
  public email!: number;
  public password!: string;
  public username!: string;
  public fullname!: string;
  public admin!: boolean;
  public logged_on!: boolean;
  public campus_id!: number;
  public city_id!: number;
}

Users.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false,
    },
    email: {
      type: new DataTypes.STRING(255),
      allowNull: false,
    },
    password: {
      type: new DataTypes.STRING(255),
      allowNull: false,
    },
    username: {
      type: new DataTypes.STRING(255),
      allowNull: false,
    },
    fullname: {
      type: new DataTypes.STRING(255),
      allowNull: false,
    },
    admin: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    campus_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    city_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    logged_on: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  },
  {
    tableName: "users",
    sequelize,
  }
);
