export interface UsersRequest {
  email: string;
  password: string;
  username: string;
  fullname: string;
  admin: boolean;
  campusId: number;
  cityId: number;
}
