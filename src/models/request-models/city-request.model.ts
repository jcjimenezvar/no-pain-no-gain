export interface CityRequest {
  cityName: string;
  cityId: number;
  email: string;
}
