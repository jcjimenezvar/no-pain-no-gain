export interface CampusRequest {
  campusName: string;
  campusId: number;
  campusAddress: string;
  cityId: number;
  email: string;
}
