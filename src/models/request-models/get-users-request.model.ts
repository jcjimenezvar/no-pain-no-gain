export interface GetUsersRequest {
  campusId: number;
  cityId: number;
  email: string;
}
