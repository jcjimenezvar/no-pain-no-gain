### TechMahidra No Pain No Gain Node Express project

Este proyecto es desarrollado con NodeJS y Typescript, Docker, Inversion de Control e Inyección de dependencias.

### Configuración y despliegue

Para iniciar el proyecto es necesario instalar las siguientes dependencias:
- Docker (https://www.docker.com/products/docker-desktop)
- NodeJs (https://nodejs.org/es/download/)

Una vez instaladas puedes clonar el repositorio de (https://gitlab.com/jcjimenezvar/no-pain-no-gain.git) utilizando el comando `git clone https://gitlab.com/jcjimenezvar/no-pain-no-gain.git` cuando el repositorio sea clonado, se debe utilzar el siguiente comando en la terminal(CMD) `cd no-pain-no-gain`. Allí se debe ejecutar el comando `npm i` el cual instalara las dependencias necesarias para que el proyecto funcione, una vez la instalación termine se deben ejecutar los siguientes comandos `docker-compose build` y `docker-compose up` o `docker-compose up -d` este ultimo para liberar la terminal. Una vez finalice la creación de la imagen estara para realizar las peticiones necesarias, de acuerdo con la documentación de las api's que encontraras en `http:localhost:4000/api-docs` 

### Pruebas Unitarias
Las pruebas unitarias estan desarrolladas con `jest` y se ejecutan con el comando `npm run test`